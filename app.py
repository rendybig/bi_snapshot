from __future__ import division
from dao.mysql import *
from services import *
import datetime


def main(event, context):

    # refresh session
    try:
        Product_Session.commit()
        IT_Session.commit()
    except:
        Product_Session.rollback()
        IT_Session.rollback()

    if event.get('detail-type') == 'Scheduled Event':
        for source in event.get('resources'):
            if 'rule/AccountSnapshot' in source:
                get_account_snapshot()
            if 'rule/DeviceSnapshot' in source:
                get_device_snapshot()


def get_account_snapshot():
    # decide which group to snapshot
    date = datetime.date.today() - datetime.timedelta(days=1)
    r = IT_Session.query(it.AccountSnapshot) \
        .filter(it.AccountSnapshot.date == date) \
        .order_by(func.substr(it.AccountSnapshot.account_id, -1).desc()).first()

    if r is None:
        x = '0'
    elif r.account_id[-1] == 'Z':
        return
    elif r.account_id[-1] == '9':
        x = 'A'
    else:
        x = chr(ord(r.account_id[-1]) + 1)

    Product_Session.execute("""
        SET group_concat_max_len=15000;
        """)
    tmp = Product_Session.execute("""
        SELECT 1 AS format_version,
        SUBDATE(CURRENT_DATE,1) AS dt,
        account.id AS account_id,
        email,
        phone,
        locale,
        account.created_time,
        usage_count AS referral_usage_count,
        subscription.status,
        service_plan.name AS last_service_plan_name,
        service_plan.extend_days AS last_service_plan_extend_days,
        cent_amount AS last_service_plan_cent_amount,
        currency AS last_service_plan_currency,
        GROUP_CONCAT(DISTINCT binding.device_id) AS device_ids,
        GROUP_CONCAT(DISTINCT mobile.id) AS mobile_ids
        FROM account
        LEFT JOIN referral_code ON account.id = referral_code.account_id
        LEFT JOIN (SELECT account_id, max(id) AS id FROM subscription GROUP BY 1) AS sub ON account.id = sub.account_id
        LEFT JOIN subscription ON subscription.id = sub.id
        LEFT JOIN service_plan ON service_plan.id = subscription.service_plan_id
        LEFT JOIN binding ON account.id = binding.account_id
        LEFT JOIN mobile ON account.id = mobile.account_id
        WHERE binding.status = '2'
        AND account.created_time < CURRENT_DATE
        AND SUBSTRING(account.id,-1) = '{}'
        GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12;
    """.format(str(x)))
    for r in tmp:
        rtomysql = it.AccountSnapshot(
            format_version=r[0], date=r[1], account_id=r[2], email=r[3], phone=r[4], locale=r[5], created_time=r[6],
            referral_usage_count=r[7], status=r[8], last_service_plan_id=r[9], last_service_plan_extend_days=r[10],
            last_service_plan_cent_amount=r[11], last_service_plan_currency=r[12], device_ids=r[13], mobile_ids=r[14]
        )
        key = IT_Session.query(it.AccountSnapshot).filter(
            (it.AccountSnapshot.format_version == rtomysql.format_version) & (it.AccountSnapshot.date == rtomysql.date)
            & (it.AccountSnapshot.account_id == rtomysql.account_id)
        )
        if key.first() is None:
            IT_Session.add(rtomysql)
        else:
            key.update({
                'email': rtomysql.email,
                'phone': rtomysql.phone,
                'locale': rtomysql.locale,
                'created_time': rtomysql.created_time,
                'referral_usage_count': rtomysql.referral_usage_count,
                'status': rtomysql.status,
                'last_service_plan_id': rtomysql.last_service_plan_id,
                'last_service_plan_extend_days': rtomysql.last_service_plan_extend_days,
                'last_service_plan_cent_amount': rtomysql.last_service_plan_cent_amount,
                'last_service_plan_currency': rtomysql.last_service_plan_currency,
                'device_ids': rtomysql.device_ids,
                'mobile_ids': rtomysql.mobile_ids,

            })
    IT_Session.commit()


def get_device_snapshot():
    # decide which group to snapshot
    date = datetime.date.today() - datetime.timedelta(days=1)
    r = IT_Session.query(it.DeviceSnapshot) \
        .with_entities(func.substr(it.DeviceSnapshot.device_id, -1).label('A')) \
        .filter(it.DeviceSnapshot.date == date) \
        .order_by(func.substr(it.DeviceSnapshot.device_id, -1).desc()).first()

    idxes = Product_Session.query(product.Device) \
        .with_entities(func.substr(product.Device.id, -1).label('A')) \
        .distinct() \
        .order_by(func.substr(product.Device.id, -1)).all()

    if r is None:
        x = idxes[0].A
    elif r.A == idxes[-1].A:
        return
    else:
        x = idxes[idxes.index(r)+1].A

    tmp = Product_Session.execute("""
        SELECT 1 AS format_version,
        SUBDATE(CURRENT_DATE,1) AS dt,
        device.id,
        device.type,
        status,
        firmware_version,
        library_version,
        device.created_time,
        last_registered_time,
        time_zone,
        product_id,
        product.name AS product_name,
        GROUP_CONCAT(device_alert_setting.name) AS enable_alerts
        FROM device
        LEFT JOIN device_info ON device.id = device_info.device_id
        LEFT JOIN product ON device.product_id = product.id
        LEFT JOIN device_alert_setting ON device.id = device_alert_setting.device_id
        WHERE device_alert_setting.on = '1'
        AND device.created_time < CURRENT_DATE
        AND last_registered_time < CURRENT_DATE
        AND SUBSTRING(device.id,-1) = '{}'
        GROUP BY 1,2,3,4,5,6,6,7,8,9,10,11;
    """.format(str(x)))
    for r in tmp:
        rtomysql = it.DeviceSnapshot(
            format_version=r[0], date=r[1], device_id=r[2], type=r[3], status=r[4], firmware_version=r[5],
            library_version=r[6], created_time=r[7], last_registered_time=r[8], timezone=r[9], product_id=r[10],
            product_name=r[11], enable_alerts=r[12]
        )
        key = IT_Session.query(it.DeviceSnapshot).filter(
            (it.DeviceSnapshot.format_version == rtomysql.format_version) & (it.DeviceSnapshot.date == rtomysql.date)
            & (it.DeviceSnapshot.device_id == rtomysql.device_id)
        )
        if key.first() is None:
            IT_Session.add(rtomysql)
        else:
            key.update({
                'type': rtomysql.type,
                'status': rtomysql.status,
                'firmware_version': rtomysql.firmware_version,
                'library_version': rtomysql.library_version,
                'created_time': rtomysql.created_time,
                'last_registered_time': rtomysql.last_registered_time,
                'timezone': rtomysql.timezone,
                'product_id': rtomysql.product_id,
                'product_name': rtomysql.product_name,
                'enable_alerts': rtomysql.enable_alerts
            })
    IT_Session.commit()
