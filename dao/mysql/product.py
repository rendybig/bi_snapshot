from services import Product_SqlBase
from sqlalchemy import Column, String, DateTime, SmallInteger, text, Integer, func, Date, Float, ForeignKey, DATETIME
from sqlalchemy.orm import relationship, backref


class Account(Product_SqlBase):
    __tablename__ = 'account'

    id = Column(String(45), unique=True, primary_key=True, default=func.generate_account_id())
    # Cognito ID is formed as [AWS Region]:[36 char UUID]
    # since currently maximum region length is 14
    # asked more space for buffering
    cognito_id = Column(String(64), unique=True)
    email = Column(String(100), unique=True)
    phone = Column(String(20), unique=True)
    locale = Column(String(45))
    password = Column(String(256))
    type = Column(Integer)
    status = Column(Integer)
    is_in_app_survey_received = Column(Integer)

    bindings = relationship("Binding", back_populates="account")


class Binding(Product_SqlBase):
    __tablename__ = 'binding'
    device_id = Column(String(12), ForeignKey('device.id'), primary_key=True)
    account_id = Column(String(45), ForeignKey('account.id'), primary_key=True)
    role = Column(Integer)

    # Currently no used
    # type = Column(Integer)

    status = Column(Integer)
    permission = Column(String(100))

    account = relationship("Account", back_populates="bindings")
    device = relationship("Device", back_populates="bindings")


class Product(Product_SqlBase):
    __tablename__ = 'product'
    id = Column(String(10), unique=True, primary_key=True)
    name = Column(String(45))
    type = Column(Integer)
    seat = Column(Integer)


class DeviceStatus(object):
    ACTIVE = 1
    RELEASED = 2
    LOCKED = 3


class Device(Product_SqlBase):
    __tablename__ = 'device'

    DEVICE_ID_LENGTH = 12
    id = Column(String(DEVICE_ID_LENGTH), unique=True, primary_key=True)
    product_id = Column(String(10), ForeignKey('product.id'))
    p2p_uuid = Column(String(45))
    device_account = Column(String(45))  # admin account id
    device_password = Column(String(45))
    status = Column(Integer)
    name = Column(String(100))
    v1_access_token = Column(String(128))

    firmware_version = Column(String(45))
    library_version = Column(String(45))
    recommend_firmware_version = Column(String(45))
    recommend_library_version = Column(String(45))
    auth_token = Column(String(255))
    last_auth_token_updated_time = Column(DATETIME)
    last_registered_time = Column(DATETIME)

    created_time = Column(DATETIME)

    product = relationship('Product', lazy='joined')
    bindings = relationship("Binding", back_populates="device")


class Service(Product_SqlBase):
    __tablename__ = 'service'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45))


class LicenseEntityType(object):
    DEVICE = 1
    ACCOUNT = 2
    BOTH = 3


class ProductServicePlan(Product_SqlBase):
    __tablename__ = 'product_service_plan'
    service_plan_id = Column(Integer, ForeignKey('service_plan.id'), primary_key=True)
    product_id = Column(String(10), ForeignKey('product.id'), primary_key=True)


class ServicePlanType(object):
    TRIAL = 1
    PRE_PAID = 2
    SUBSCRIPTION = 3
    ONE_TIME = 4
    TRIAL_UNLIMITED = 5


class ServicePlan(Product_SqlBase):
    __tablename__ = 'service_plan'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45))
    service_id = Column(Integer, ForeignKey('service.id'))
    license_entity_type = Column(Integer)
    license_type = Column(Integer)
    extend_days = Column(Integer)
    grace_period_days = Column(Integer)
    android_bundle_id = Column(String(255))
    ios_bundle_id = Column(String(255))

    service = relationship("Service")
    product_service_plans = relationship("ProductServicePlan", backref=backref('product_service_plan'))
    # device_subscription = relationship("DeviceSubscription", back_populates="service_plan")


class SubscriptionStatus(object):
    Active = 1
    Cancelled = 2
    GracePeriod = 3
    Expired = 4
    Converted = 5


class Subscription(Product_SqlBase):
    __tablename__ = 'subscription'

    id = Column(Integer, primary_key=True)
    account_id = Column(ForeignKey('account.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    service_plan_id = Column(ForeignKey('service_plan.id', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, index=True)
    status = Column(Integer)
    expiration_date = Column(DateTime)
    type = Column(Integer)
    receipt_token = Column(String(40480))
    created_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    updated_time = Column(DateTime)
    platform = Column(String(55))

    account = relationship('Account')
    service_plan = relationship('ServicePlan')
    subscription_devices = relationship("SubscriptionDevice", back_populates="subscription")


class SubscriptionDevice(Product_SqlBase):
    __tablename__ = 'subscription_device'

    subscription_id = Column(ForeignKey('subscription.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False, index=True)
    device_id = Column(ForeignKey('device.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False)
    status = Column(Integer)
    type = Column(Integer)
    created_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    updated_time = Column(DateTime)

    device = relationship('Device')
    subscription = relationship('Subscription')


class ReferralCode(Product_SqlBase):
    __tablename__ = 'referral_code'
    id = Column(String(45), primary_key=True)
    account_id = Column(String(45), ForeignKey('account.id'))
    naming_group = Column(String(255))
    sequence = Column(Integer)
    usage_count = Column(Integer)
    created_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    account = relationship("Account")

    def __init__(self, id, account_id, naming_group, sequence):
        self.id = id
        self.account_id = account_id
        self.naming_group = naming_group
        self.sequence = sequence
        self.usage_count = 0

    def __repr__(self):
        return '<ReferralCode: {id} /account_id: {account_id}>'.format(
            id=self.id,
            account_id=self.account_id
        )


class Referral(Product_SqlBase):
    __tablename__ = 'referral'
    account_id = Column(String(45), ForeignKey('account.id'), primary_key=True)
    order_id = Column(String(255), primary_key=True)
    order_source = Column(String(30), primary_key=True)
    referee = Column(String(255))
    referral_code = Column(String(45))
    redeemed_times = Column(Integer)
    expiration_time = Column(DateTime)
    created_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    updated_time = Column(DateTime)

    account = relationship("Account")

    def __init__(self, account_id, order_id, order_source, referee, expiration_time, referral_code=None):
        self.order_id = order_id
        self.account_id = account_id
        self.order_source = order_source
        self.referee = referee
        self.redeemed_times = 0
        self.expiration_time = expiration_time
        self.referral_code = referral_code

    def __repr__(self):
        return '<Referral: {account_id} /order_id: {order_id} /order_source: {order_source}>'.format(
            account_id=self.account_id,
            order_id=self.order_id,
            order_source=self.order_source
        )
