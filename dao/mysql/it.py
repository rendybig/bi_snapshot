from services import IT_SqlBase
from sqlalchemy import Column, String, DateTime, SmallInteger, text, Integer, func, Date, Float


class AccountSnapshot(IT_SqlBase):
    __tablename__ = 'account_snapshot'

    format_version = Column(Integer, primary_key=True, nullable=False)
    date = Column(Date, primary_key=True, nullable=False)
    account_id = Column(String, primary_key=True, nullable=False)
    email = Column(String)
    phone = Column(String)
    locale = Column(String)
    created_time = Column(DateTime)
    referral_usage_count = Column(Integer)
    status = Column(Integer)
    last_service_plan_id = Column(String)
    last_service_plan_extend_days = Column(Integer)
    last_service_plan_cent_amount = Column(Integer)
    last_service_plan_currency = Column(String)
    device_ids = Column(String)
    mobile_ids = Column(String)

    def __init__(self, format_version, date, account_id, email, phone, locale, created_time, referral_usage_count,
                 status, last_service_plan_id, last_service_plan_extend_days, last_service_plan_cent_amount,
                 last_service_plan_currency, device_ids, mobile_ids):
        self.format_version = format_version
        self.date = date
        self.account_id = account_id
        self.email = email
        self.phone = phone
        self.locale = locale
        self.created_time = created_time
        self.referral_usage_count = referral_usage_count
        self.status = status
        self.last_service_plan_id = last_service_plan_id
        self.last_service_plan_extend_days = last_service_plan_extend_days
        self.last_service_plan_cent_amount = last_service_plan_cent_amount
        self.last_service_plan_currency = last_service_plan_currency
        self.device_ids = device_ids
        self.mobile_ids = mobile_ids


class DeviceSnapshot(IT_SqlBase):
    __tablename__ = 'device_snapshot'

    format_version = Column(Integer, primary_key=True, nullable=False)
    date = Column(Date, primary_key=True, nullable=False)
    device_id = Column(String, primary_key=True, nullable=False)
    type = Column(SmallInteger)
    status = Column(SmallInteger)
    firmware_version = Column(String)
    library_version = Column(String)
    created_time = Column(DateTime)
    last_registered_time = Column(DateTime)
    timezone = Column(String)
    product_id = Column(String)
    product_name = Column(String)
    enable_alerts = Column(String)

    def __init__(self, format_version, date, device_id, type, status, firmware_version, library_version, created_time,
                 last_registered_time, timezone, product_id, product_name, enable_alerts):
        self.format_version = format_version
        self.date = date
        self.device_id = device_id
        self.type = type
        self.status = status
        self.firmware_version = firmware_version
        self.library_version = library_version
        self.created_time = created_time
        self.last_registered_time = last_registered_time
        self.timezone = timezone
        self.product_id = product_id
        self.product_name = product_name
        self.enable_alerts = enable_alerts
