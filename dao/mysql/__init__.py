from sqlalchemy import Column, String, Integer, ForeignKey, DATE, text
from sqlalchemy.orm import relationship
from services import Product_SqlBase, Product_Session, IT_Session
from .it import *
from .product import *


def execute_raw_sql(session, sql):
    result = session.execute(sql)
    return result


